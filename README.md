# powerctl

`powerctl` is a wrapper script to control the user session and the systmes' 
power state. This tries to be portable across systems and init systems. Where
possible, it tries to use the underlying init system because that is more likely
to allow the system's power state to be changed without requiring elevated
privileges.

# Requirements

`powerctl` is written in Lua and requires a Lua interpreter and some Lua 
packages. 

## Interpreter

Lua >= 5.2

## Packages

These can be installed using [LuaRocks](https://luarocks.org). They may 
also be present in the system's repositories.

- [argparse](https://luarocks.org/modules/argparse/argparse)

# Installation

The `powerctl` script can be downloaded separately and copied to a directory 
that is in $PATH. 

# Usage

Run the following command on the terminal to see the list of commands and 
options available

```
powerctl -h
```
